//
//  SubRowTableViewCell.h
//  Joey
//
//  Created by Sujan on 8/24/16.
//  Copyright © 2016 Sujan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ResponsiveLabel.h"

@interface SubRowTableViewCell : UITableViewCell


@property (weak, nonatomic) IBOutlet ResponsiveLabel *answerLabel;

@end
